// Copyright 2020 Sander Bollen <sander@sndrtj.eu>
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
// conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
// of conditions and the following disclaimer in the documentation and/or other materials
// provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific prior
// written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
// SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use clap::{App, Arg};
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::stdout;
use std::io::{BufWriter, Write};
use std::ops;
use std::path::Path;
use std::sync::mpsc;
use std::thread;

/// Small value (1cm) used to prevent singularities.
const SMALL_VALUE: f64 = 0.01; // 1cm

/// Gravitational constant.
const G: f64 = 6.674e-11;

/// A Vector3D represents a vector of floats in 3 dimensions: x, y, and z.
/// We use Vector3D for representing coordinates, velocity vectors, force vectors, and
/// acceleration vectors.
#[derive(Serialize, Deserialize, Debug)]
struct Vector3D {
    x: f64,
    y: f64,
    z: f64,
}

impl Vector3D {
    /// Calculate the magnitude of a Vector3D.
    fn magnitude(&self) -> f64 {
        (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
    }

    /// Calculate the euclidean distance between this Vector3D and another Vector3D.
    fn distance(&self, other: &Vector3D) -> f64 {
        ((self.x - other.x).powi(2) + (self.y - other.y).powi(2) + (self.z - other.z).powi(2))
            .sqrt()
    }
}

impl Clone for Vector3D {
    fn clone(&self) -> Vector3D {
        Vector3D {
            x: self.x,
            y: self.y,
            z: self.z,
        }
    }
}

impl ops::Add<&Vector3D> for &Vector3D {
    type Output = Vector3D;

    fn add(self, _rhs: &Vector3D) -> Vector3D {
        Vector3D {
            x: self.x + _rhs.x,
            y: self.y + _rhs.y,
            z: self.z + _rhs.z,
        }
    }
}

impl ops::Add<Vector3D> for Vector3D {
    type Output = Vector3D;

    fn add(self, _rhs: Vector3D) -> Vector3D {
        Vector3D {
            x: self.x + _rhs.x,
            y: self.y + _rhs.y,
            z: self.z + _rhs.z,
        }
    }
}

impl ops::Add<f64> for Vector3D {
    type Output = Vector3D;

    fn add(self, _rhs: f64) -> Vector3D {
        Vector3D {
            x: self.x + _rhs,
            y: self.y + _rhs,
            z: self.z + _rhs,
        }
    }
}

impl ops::Sub<&Vector3D> for &Vector3D {
    type Output = Vector3D;

    fn sub(self, _rhs: &Vector3D) -> Vector3D {
        Vector3D {
            x: self.x - _rhs.x,
            y: self.y - _rhs.y,
            z: self.z - _rhs.z,
        }
    }
}

impl ops::Sub<f64> for Vector3D {
    type Output = Vector3D;

    fn sub(self, _rhs: f64) -> Vector3D {
        Vector3D {
            x: self.x - _rhs,
            y: self.y - _rhs,
            z: self.z - _rhs,
        }
    }
}

impl ops::Mul<f64> for &Vector3D {
    type Output = Vector3D;

    fn mul(self, _rhs: f64) -> Vector3D {
        Vector3D {
            x: self.x * _rhs,
            y: self.y * _rhs,
            z: self.z * _rhs,
        }
    }
}

/// A PhysicalObject represents an object in space, with coordinates of its center, a velocity
/// vector, some mass, and a name. We do not (yet) implement a radius, hence PhysicalObjects
/// are modelled as infinitely dense points in space.
///
/// The center coordinates are expected to be provided in units of one meter, with respect so
/// some reference point. The velocity vector is expected to be provided in units of one m/s, and
/// the mass is expected to be provided in units of one kg.
#[derive(Serialize, Deserialize, Debug)]
struct PhysicalObject {
    center: Vector3D,   // in m
    velocity: Vector3D, // in m/s
    mass: f64,          // in kg
    name: String,
}

impl PhysicalObject {
    fn to_string(&self) -> String {
        format!(
            "{name}\t{mass}\t{center_x}\t{center_y}\t{center_z}\t{velocity_x}\
        \t{velocity_y}\t{velocity_z}",
            name = self.name,
            mass = self.mass,
            center_x = self.center.x,
            center_y = self.center.y,
            center_z = self.center.z,
            velocity_x = self.velocity.x,
            velocity_y = self.velocity.y,
            velocity_z = self.velocity.z
        )
    }
}

impl Clone for PhysicalObject {
    fn clone(&self) -> PhysicalObject {
        PhysicalObject {
            center: self.center.clone(),
            velocity: self.velocity.clone(),
            mass: self.mass,
            name: self.name.clone(),
        }
    }
}

/// A simulation consists of a Vec of PhysicalObjects, some number of simulated seconds (iterations)
/// for which we should simulate, and a write interval which controls how often we write the
/// simulation state to stdout.
///
/// We expect each `PhysicalObject` to have a unique name across the entire simulation.
/// If this is not the case, you can expect some unexpected behaviour.
#[derive(Serialize, Deserialize, Debug)]
struct Simulation {
    objects: Vec<PhysicalObject>,
    seconds: u64,        // max is approximately 584 billion years.
    write_interval: u64, // controls how often we write the result to stdout.
}

impl Simulation {
    /// Runs the simulation.
    ///
    /// The Simulations must have been bound to a mutable variable.
    ///
    /// Writes the simulation state to stdout each `write_interval` iterations.
    fn simulate(&mut self, tx: std::sync::mpsc::Sender<Vec<(u64, PhysicalObject)>>) {
        let mut iter_obj = self.objects.to_vec();

        let mut send_buffer: Vec<(u64, PhysicalObject)> = Vec::with_capacity(4096);

        for i in 0..self.seconds {
            if i % self.write_interval == 0 {
                if send_buffer.len() % 4096 == 0 {
                    let v = send_buffer.clone();
                    tx.send(v).unwrap();
                    send_buffer.clear();
                }
                for item in &iter_obj {
                    let w = item.clone();
                    &send_buffer.push((i, w));
                }
            }

            iter_obj = simulation_round(iter_obj);
            self.objects = iter_obj.to_vec();
        }

        // One last time.
        tx.send(send_buffer).unwrap();
    }

    fn new(objects: Vec<PhysicalObject>, seconds: u64, write_interval: u64) -> Simulation {
        Simulation {
            objects,
            seconds,
            write_interval,
        }
    }
}

/// Compute one round of simulation.
///
/// Takes a `Vec` of `PhysicalObject`s, and produces a new `Vec` of equal length, though
/// not guaranteed same order.
///
/// One round of simulation is assumed to represent one simulated second.
fn simulation_round(objects: Vec<PhysicalObject>) -> Vec<PhysicalObject> {
    let capacity = objects.len();
    // Not specifying the capacity results in a > 10% slowdown of the entire application.
    let mut result: Vec<PhysicalObject> = Vec::with_capacity(capacity);
    // Not specifying the capacity results in another > 10% slowdown of the entire application
    let mut accelerations: HashMap<&String, Vec<Vector3D>> = HashMap::with_capacity(capacity);

    // Calculate the effects of all interactions.
    for (obj_one, obj_two) in objects.iter().tuple_combinations() {
        let (accel_one, accel_two) = calc_acceleration_vectors(&obj_one, &obj_two);

        accelerations
            .entry(&obj_one.name)
            .or_insert(Vec::with_capacity(capacity))
            .push(accel_one);
        accelerations
            .entry(&obj_two.name)
            .or_insert(Vec::with_capacity(capacity))
            .push(accel_two);
    }

    for obj in &objects {
        // We can safely assume all objects are in the hashmap.
        let accs = accelerations.get(&obj.name).unwrap();

        // add up all interactions for the same object, and add to the already existing velocity.
        let new_velocity = accs.iter().fold(obj.velocity.clone(), |acc, x| &acc + x);
        // Compute new coordinate based on velocity vector and old coordinate.
        let new_coordinate = &obj.center + &new_velocity;

        result.push(PhysicalObject {
            name: obj.name.clone(),
            mass: obj.mass,
            velocity: new_velocity,
            center: new_coordinate,
        })
    }

    return result;
}

/// Calculate acceleration vectors for references to a pair of `PhysicalObject`s.
///
/// Returns a tuple of acceleration vectors for one second of simulated gravitational interaction,
/// one vector for each `PhysicalObject`.
///
/// Returned vectors are in units of m/s^2.
///
/// Example:
///
/// ```
/// let earth_pos = Vector3D { x: 0, y: 0, z: 0};
/// let earth_velocity = Vector3D { x: 0, y: 0, z: 0};
/// let person_pos = Vector3D { x: 0, y: 0, z: 100};
/// let person_velocity = Vector3D {x:0, y:0, z:0 };
/// let earth = PhysicalObject(center: earth_pos, velocity: earth_velocity, mass: 5.97237e+24, name: "Earth");
/// let person = PhysicalObject(center: person_pos, velocity: person_velocity, mass: 80, name: "Person");
///
/// let (earth_accel, person_accel) = calc_acceleration_vectors(&earth, &person);
///
/// println!("{:.2}", person_accel.magnitude());
/// > 9.81
///
/// println!("{}", earth_accel.magnitude());
/// > Some very small number.
/// ```
///
/// To prevent singularities, the minimum distance between two objects is forced to 1 cm.
fn calc_acceleration_vectors(
    phys_obj: &PhysicalObject,
    phys_obj2: &PhysicalObject,
) -> (Vector3D, Vector3D) {
    let mut r = phys_obj.center.distance(&phys_obj2.center);

    if r <= 0.0 {
        r = SMALL_VALUE;
    }

    let force = G * ((phys_obj.mass * phys_obj2.mass) / r.powi(2));

    let a1 = force / phys_obj.mass;
    let a2 = force / phys_obj2.mass;

    let direction1 = &phys_obj.center - &phys_obj2.center;
    let direction2 = &phys_obj2.center - &phys_obj.center;

    let unit_1 = &(&direction1 * (1.0 / direction1.magnitude())) * -1.0;
    let unit_2 = &(&direction2 * (1.0 / direction2.magnitude())) * -1.0;

    let v1 = &unit_1 * a1;
    let v2 = &unit_2 * a2;

    return (v1, v2);
}

fn main() {
    let cli = App::new("gravsim")
        .version("0.1.0")
        .author("Sander Bollen <sander@sndrtj.eu>")
        .about("Little numerical gravity simulator")
        .arg(
            Arg::with_name("params")
                .help(
                    "Path to file containing simulation parameters in JSON format. \
            See 'examples' directory for several examples.",
                )
                .required(true)
                .index(1),
        );

    let matches = cli.get_matches();

    // "params" was set to required, so we can safely call unwrap.
    let params_path = matches.value_of("params").unwrap();

    let path = Path::new(params_path);
    let pname = path.display();

    let mut file = match File::open(&path) {
        Err(why) => panic!("Could not open {} due to: {}", pname, why),
        Ok(file) => file,
    };

    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Err(why) => panic!("could not read file {} due to: {}", pname, why),
        Ok(_) => (),
    }

    let mut simulation: Simulation = match serde_json::from_str(&contents) {
        Err(why) => panic!("Could not load simulation due to: {}", why),
        Ok(simulation) => simulation,
    };

    // We use a channel, with the simulation in a separate thread, so we only have to send
    // This is because calling !format is actually quite expensive, which is especially noticeable
    // with low write eps. For high write eps this approach is slightly slower than the naive
    // implementation.
    let (tx, rx): (
        std::sync::mpsc::Sender<Vec<(u64, PhysicalObject)>>,
        std::sync::mpsc::Receiver<Vec<(u64, PhysicalObject)>>,
    ) = mpsc::channel();
    let mut writer = BufWriter::new(stdout());

    thread::spawn(move || simulation.simulate(tx));

    for message in rx {
        for (idx, physobj) in message {
            writer
                .write(format!("{}\t{}\n", idx, physobj.to_string()).as_bytes())
                .unwrap();
        }
    }
    writer.flush().unwrap();
}
