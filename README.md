gravsim
=======

Little numerical gravitational simulator.

## How to install

`cargo build --release`

## How to use.

Create a JSON file with the parameters of your simulation.
See [examples](/examples) for several examples. At minimum this should contain:

1. Two objects in space with coordinates and some initial velocity, mass and a _unique_ name.
2. A number of simulation seconds for which you want to simulate. If you e.g. want to simulate
2 earth years, that number is `63072000`.
3. A write interval; this controls how often the simulation state will be printed
to stdout. If set to 1, this means every single simulated second. If set to 3600, this means
once per simulated hour. 


### How to get coordinates?

You can get current state vectors (coordinates and velocity) for various bodies in our solar
system from the excellent [NASA HORIZONS](https://ssd.jpl.nasa.gov/horizons.cgi#top) 
tool. See [this](https://space.stackexchange.com/questions/25256/getting-state-vectors-from-jpl-horizons-ephemerides/25357#25357)
stack exchange answer for a detailed explanation of how to use this tool. Make sure you convert the units to meters and m/s, respectively. By default
HORIZONS returns units of au/day, but you can change this to km and km/s respectively.

### Output format

Each `write_interval` iterations, one row of tab-separated values will be written
to stdout that represents the then-current simulation state. This row has the following
fields:

1. n iterations / seconds in simulation
2. Name of object
3. Mass of object in kg
4. X coordinate in m
5. Y coordinate in m
6. Z coordinate in m
7. X velocity component in m/s
8. Y velocity component in m/s
9. Z velocity component in m/s
 

### What you can expect performance wise?

The following measurements were taken on an AMD Ryzen 2400G, using 
[hyperfine](https://github.com/sharkdp/hyperfine), while simultaneously
running an IDE, an open Firefox window with about 25 tabs, and 3 instances
of Libreoffice Calc.

1. A 2.77-hour simulation of a person-sized object orbiting the earth,
with a write interval of 1 second.

```
Benchmark #1: target/release/gravsim examples/earth_person.json > /dev/null
  Time (mean ± σ):      38.0 ms ±   2.8 ms    [User: 43.8 ms, System: 6.3 ms]
  Range (min … max):    31.2 ms …  45.6 ms    79 runs

```

2. A 3-month simulation of the Earth and the Moon, with a write interval of 
60 seconds. 

```
Benchmark #1: target/release/gravsim examples/earth_moon.json > /dev/null
  Time (mean ± σ):      2.560 s ±  0.031 s    [User: 3.280 s, System: 0.007 s]
  Range (min … max):    2.516 s …  2.615 s    10 runs
```

3. A 2-year long simulation of the Sun, Earth and the Moon, with a write interval
of 3600 seconds:

```
Benchmark #1: target/release/gravsim examples/sun_earth_moon.json > /dev/null
  Time (mean ± σ):     36.705 s ±  0.682 s    [User: 36.768 s, System: 0.012 s]
  Range (min … max):   36.022 s … 38.189 s    10 runs
```


## Known caveats

1. Objects are currently modelled as infinitely dense points in space. To
prevent singularities, the minimum distance between two objects is forced to
1 cm. This also means the gravitational field _inside_ objects is not modelled
correctly. E.g., an object that would fall towards the center of the Earth
would experience ever greater acceleration the closer it gets to the Earth's core,
even when a significant portion of Earth's mass would now be behind the object.
We hope to address this in a future release.
2. We use simple float arithmetic. This is guaranteed to yield rounding errors,
whose effects may grow to exponential proportions over large time scales and
distances.  
3. Objects cannot collide with one another.
4. We use a Newtonian model of gravity. This suffices for objects within our
Solar system, but don't use this tool to model interactions at relativistic
speeds. 


## Usage

```
gravsim 0.1.0
Sander Bollen <sander@sndrtj.eu>
Little numerical gravity simulator

USAGE:
    gravsim <params>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <params>    Path to file containing simulation parameters in JSON format. See 'examples' directory for several
                examples.

```

## License

BSD-3-clause
