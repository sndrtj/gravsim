import argparse
import pathlib
from typing import List, Tuple
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d.art3d import Line3D
import tqdm

HEADER = [
    "seconds",
    "name",
    "mass",
    "center_x",
    "center_y",
    "center_z",
    "velocity_x",
    "velocity_y",
    "velocity_z",
]


def animate(
    input_path: pathlib.Path, step_size: int, fps: int = 30, dpi: int = 300
) -> (plt.Figure, animation.FuncAnimation):
    """Animate

    :param input_path:
    :param step_size:
    :param fps:
    :return: a tuple of figure and animation. We return the animation, because a
        reference to this object must be kept until the figure is actually rendered.
    """
    df = pd.read_csv(input_path, sep="\t", header=None, names=HEADER)
    # We want a neat cube as as bounding box, so we find out whatever the maximum
    # and minimum across all axes.
    min_val = df[["center_x", "center_y", "center_z"]].min().min()
    max_val = df[["center_x", "center_y", "center_z"]].max().max()

    # Split by name. List of (name, df) tuples
    groups: List[Tuple[str, pd.DataFrame]] = list(df.groupby("name"))

    fig = plt.figure(dpi=dpi)
    ax = fig.add_subplot(projection="3d")
    ax.set_xlim3d([min_val, max_val])
    ax.set_xlabel("X")
    ax.set_ylim3d([min_val, max_val])
    ax.set_ylabel("Y")
    ax.set_zlim3d([min_val, max_val])
    ax.set_zlabel("Z")

    # Create lines for each object. Note that we cannot pass empty arrays to plot,
    # so we have to plot the first element. We store a tuple of line objects for each
    # group; the first item is to be used an actual line, whereas the second object is
    # to be used for a point only.
    plots: List[Tuple[Line3D, Line3D]] = []
    for group_name, group_df in groups:
        line = ax.plot(
            group_df["center_x"].iloc[0:1],
            group_df["center_y"].iloc[0:1],
            group_df["center_z"].iloc[0:1],
            alpha=0.3,
        )[0]
        point = ax.plot(
            group_df["center_x"].iloc[0:1],
            group_df["center_y"].iloc[0:1],
            group_df["center_z"].iloc[0:1],
            "o",
            alpha=0.8,
            color=line.get_color(),
            label=group_name,
            markersize=4
        )[0]
        plots.append((line, point))

    # Get group arrays, so we don't have to call .iloc in a loop.
    group_arrays = [
        (
            group_df["center_x"].values,
            group_df["center_y"].values,
            group_df["center_z"].values,
        )
        for _, group_df in groups
    ]

    def update_lines(upto_it: int, lines: List[Tuple[Line3D, Line3D]]):
        for i, (line, point) in enumerate(lines):
            arrays = group_arrays[i]
            line.set_data_3d(
                arrays[0][:upto_it],
                arrays[1][:upto_it],
                arrays[2][:upto_it],
            )
            point.set_data_3d(
                arrays[0][upto_it], arrays[1][upto_it], arrays[2][upto_it]
            )
            lines[i] = (line, point)
        return lines

    steps = tqdm.tqdm(
        range(0, groups[0][1].shape[0], step_size),
        desc="Drawing frames",
        total=int(groups[0][1].shape[0] / step_size),
    )

    line_animation = animation.FuncAnimation(
        fig, update_lines, steps, fargs=(plots,), interval=int(1000 / fps)
    )
    return fig, line_animation


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--output",
        type=pathlib.Path,
        required=True,
        help="Path to output file. Required",
    )
    parser.add_argument(
        "-s",
        "--step-size",
        type=int,
        default=1,
        help=(
            "Step size for plotting, ergo how many data points per frame. "
            "Defaults to 1"
        ),
    )
    parser.add_argument(
        "input_path", metavar="input", type=pathlib.Path, help="Path to input TSV file."
    )
    args = parser.parse_args()

    fig, line_animation = animate(args.input_path, args.step_size)
    plt.legend()
    line_animation.save(str(args.output))
